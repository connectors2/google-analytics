import pandas as pd
from apiclient.discovery import build
from oauth2client.service_account import ServiceAccountCredentials
from pathlib import Path
import logging

class GoogleAnalyticsConfig:
    
    # def __init__(self, viewId):
        # self.analytics = analytics
        # self.viewId = viewId
        # self.startDate = startDate
        # self.endDate = endDate
        # self.keyFileLocation = keyFileLocation
        
        # self.analytics = initialize_analyticsreporting(self, keyFileLocation)
        
    
    def initialize_analyticsreporting(self, keyFileLocation):
        SCOPES = ['https://www.googleapis.com/auth/analytics.readonly']
        credentials = ServiceAccountCredentials.from_json_keyfile_name(keyFileLocation, SCOPES)
        # Build the service object.
        analytics = build('analyticsreporting', 'v4', credentials=credentials)

        return analytics

    """
    List of methods.
    """

    def get_events(self, keyFileLocation, startDate, endDate, viewId):
        self.keyFileLocation = keyFileLocation
        self.startDate = startDate
        self.endDate = endDate
        self.viewId = viewId
        data = {
            "DIMENSIONS": ['ga:date', 'ga:eventCategory', 'ga:eventAction', 'ga:eventLabel'],
            "METRICS":['ga:totalEvents','ga:uniqueEvents','ga:eventValue','ga:avgEventValue','ga:sessionsWithEvent','ga:eventsPerSessionWithEvent', 'ga:users']
        }
        analytics = self.initialize_analyticsreporting(keyFileLocation)
        resp = self.get_report(analytics, data, startDate, endDate, viewId)
        return self.convert_to_dataframe(resp)
    
    
    def get_landingPages(self, keyFileLocation, startDate, endDate, viewId):
        self.keyFileLocation = keyFileLocation
        self.startDate = startDate
        self.endDate = endDate
        self.viewId = viewId
        data = {
            "DIMENSIONS": ['ga:date','ga:landingPagePath', 'ga:channelGrouping', 'ga:pageTitle'],
            "METRICS":['ga:sessions' ]
        }
        analytics = self.initialize_analyticsreporting(keyFileLocation)
        resp = self.get_report(analytics, data, startDate, endDate, viewId)
        return self.convert_to_dataframe(resp)
    
    
    def get_contentGroups(self, keyFileLocation, startDate, endDate, viewId):
        self.keyFileLocation = keyFileLocation
        self.startDate = startDate
        self.endDate = endDate
        self.viewId = viewId
        data = {
            "DIMENSIONS": ['ga:date','ga:contentGroup1'],
            "METRICS":['ga:sessions' ]
        }
        analytics = self.initialize_analyticsreporting(keyFileLocation)
        resp = self.get_report(analytics, data, startDate, endDate, viewId)
        return self.convert_to_dataframe(resp)
    
    
    def get_pages(self, keyFileLocation, startDate, endDate, viewId):
        self.keyFileLocation = keyFileLocation
        self.startDate = startDate
        self.endDate = endDate
        self.viewId = viewId
        data = {
            "DIMENSIONS": ['ga:date','ga:pagePath', 'ga:pageTitle'],
            "METRICS":['ga:pageviews' ]
        }
        analytics = self.initialize_analyticsreporting(keyFileLocation)
        resp = self.get_report(analytics, data, startDate, endDate, viewId)
        return self.convert_to_dataframe(resp)
        
    """
    End list of methods
    """ 
        
    def get_report(self, analytics, data, startDate, endDate, viewId):
        return analytics.reports().batchGet(
            body={
            'reportRequests': [
            {
                'viewId': viewId,
                'dateRanges': [{'startDate': startDate, 'endDate': endDate}],
                'metrics': [{'expression':i} for i in data['METRICS']],
                'dimensions': [{'name':j} for j in data['DIMENSIONS']],
                "pageSize": 100000,
            }]
            }
        ).execute()
        
    
    def convert_to_dataframe(self, response):
        for report in response.get('reports', []):
            columnHeader = report.get('columnHeader', {})
            dimensionHeaders = columnHeader.get('dimensions', [])
            metricHeaders = [i.get('name',{}) for i in columnHeader.get('metricHeader', {}).get('metricHeaderEntries', [])]
            finalRows = []

            for row in report.get('data', {}).get('rows', []):
                dimensions = row.get('dimensions', [])
                metrics = row.get('metrics', [])[0].get('values', {})
                rowObject = {}

                for header, dimension in zip(dimensionHeaders, dimensions):
                    rowObject[header] = dimension
                
                for metricHeader, metric in zip(metricHeaders, metrics):
                    rowObject[metricHeader] = metric

                finalRows.append(rowObject)
                
            dataFrameFormat = pd.DataFrame(finalRows)    
        return dataFrameFormat 