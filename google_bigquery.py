from google.cloud import bigquery
from google.oauth2 import service_account
# from datalab.context import Context
# import datalab.storage as storage
# import datalab.bigquery as bq
import pandas as pd
import time
# import pandas_gbq
from google.cloud.exceptions import NotFound


class GoogleBQ:
    def __init__(self, project_id, keyFileLocation):
        self.project_id = project_id
        # self.dataset_id = dataset_id
        self.keyFileLocation = keyFileLocation
        self.client = self.connect(keyFileLocation, project_id)
        
    
    def connect(self, keyFileLocation, project_id):
        credentials = service_account.Credentials.from_service_account_file(
        keyFileLocation,
        scopes=["https://www.googleapis.com/auth/cloud-platform"] )
        client = bigquery.Client( credentials=credentials, project=project_id )
        
        return client
    
    def insert_data(self, data, table_id):
        
        job = self.client.load_table_from_dataframe(data, table_id )
        # Wait for the load job to complete.
        job.result()

    def create_events_table(self):
        # bigquery_client = bigquery.Client()
        dataset_ref = self.client.dataset('google_analytics')

        # Prepares a reference to the table
        table_ref = dataset_ref.table('events')

        try:
            self.client.get_table(table_ref)
        except NotFound:
            schema = [
                bigquery.SchemaField('view_id', 'INTEGER', mode='REQUIRED'),
                bigquery.SchemaField('date', 'TIMESTAMP', mode='REQUIRED'),
                bigquery.SchemaField('eventCategory', 'STRING', mode='REQUIRED'),
                bigquery.SchemaField('eventAction', 'STRING', mode='REQUIRED'),
                bigquery.SchemaField('eventLabel', 'STRING', mode='REQUIRED'),
                bigquery.SchemaField('totalEvents', 'INTEGER', mode='NULLABLE'),
                bigquery.SchemaField('uniqueEvents', 'INTEGER', mode='NULLABLE'),
                bigquery.SchemaField('eventValue', 'INTEGER', mode='NULLABLE'),
                bigquery.SchemaField('avgEventValue', 'INTEGER', mode='NULLABLE'),
                bigquery.SchemaField('sessionsWithEvent', 'INTEGER', mode='NULLABLE'),
                bigquery.SchemaField('eventsPerSessionWithEvent', 'FLOAT64', mode='NULLABLE'),
                bigquery.SchemaField('users', 'INTEGER', mode='NULLABLE'),
            ]
            table = bigquery.Table(table_ref, schema=schema)
            table = self.client.create_table(table)
            print('table {} created.'.format(table.table_id))
            
    def create_lp_table(self):
        # bigquery_client = bigquery.Client()
        dataset_ref = self.client.dataset('google_analytics')

        # Prepares a reference to the table
        table_ref = dataset_ref.table('landing_pages')

        try:
            self.client.get_table(table_ref)
        except NotFound:
            schema = [
                bigquery.SchemaField('view_id', 'INTEGER', mode='REQUIRED'),
                bigquery.SchemaField('date', 'TIMESTAMP', mode='REQUIRED'),
                bigquery.SchemaField('landingPagePath', 'STRING', mode='REQUIRED'),
                bigquery.SchemaField('channelGrouping', 'STRING', mode='REQUIRED'),
                bigquery.SchemaField('pageTitle', 'STRING', mode='REQUIRED'),
                bigquery.SchemaField('sessions', 'INTEGER', mode='NULLABLE'),
            ]
            table = bigquery.Table(table_ref, schema=schema)
            table = self.client.create_table(table)
            print('table {} created.'.format(table.table_id))


