# Reqierments
# pip install --upgrade google-api-python-client
# pip install oauth2client
# pip install pandas
# pip install pygsheets




# TODO: 
# Добавить логгирование
# Добавить конфиг файл
# Добавить абсолютные пути к конфигу и ключам
# Добавить запись в клиентский бигквери



import pandas as pd
# import pygsheets
# from apiclient.discovery import build
# from oauth2client.service_account import ServiceAccountCredentials
from pathlib import Path
import logging

import ga, google_bigquery
# import time
# import pandas_gbq
# from google.cloud import bigquery
# from google.oauth2 import service_account
import configparser
from datetime import datetime, timedelta

log_file = Path(Path(__file__).parent.absolute(),'ga-logs.log')
logging.basicConfig(format="%(asctime)s,%(msecs)d %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s", filename=log_file, level=logging.ERROR)
log = logging.getLogger(__name__)

cfg = configparser.ConfigParser()
cfg.read( Path(Path(__file__).parent.absolute(),'config.cfg') )

KEY_FILE_LOCATION = cfg.get('GA', 'KEY_FILE_LOCATION')
# VIEW_ID_vitalmtb = cfg.get('GA', 'VIEW_ID_vitalmtb')
view_ids = [cfg.get('GA', 'VIEW_ID_vitalmtb')]

ga = ga.GoogleAnalyticsConfig()
bq = google_bigquery.GoogleBQ('coronadb-276313', KEY_FILE_LOCATION)


datelist = pd.date_range(datetime.today() - timedelta(days=360), periods=360, freq='D').date.tolist()

# for date in datelist:
#     print(date.strftime('%Y-%m-%d'))

# bq.create_events_table()

# exit()

# get_landingPages = ga.get_landingPages(KEY_FILE_LOCATION, '1daysAgo', 'today', VIEW_ID_vitalmtbx)

# get_landingPages.columns = ['date','landingPagePath', 'channelGrouping', 'pageTitle', 'sessions' ]

# get_landingPages['date'] = pd.to_datetime(get_landingPages['date'], format='%Y%m%d')
# get_landingPages['sessions'] = pd.to_numeric(get_landingPages['sessions'])
# get_landingPages['view_id'] = VIEW_ID_vitalmtbx
# print(get_landingPages)
# exit()

# bq.insert_data(get_landingPages, 'google_analytics.landing_pages')

def main():
    for date in datelist:
        
        # print(date.strftime('%Y-%m-%d'))
        
        get_lp(date.strftime('%Y-%m-%d'), date.strftime('%Y-%m-%d'), view_ids[0])
    # get_lp('2021-01-25', '2021-01-25', VIEW_ID_vitalmtbx)


def get_lp(start, end, view_id):
    
    bq.create_lp_table()
    
    get_landingPages = ga.get_landingPages(KEY_FILE_LOCATION, start, end, view_id)
    
    # print(get_landingPages)

    get_landingPages.columns = ['date','landingPagePath', 'channelGrouping', 'pageTitle', 'sessions' ]

    get_landingPages['date'] = pd.to_datetime(get_landingPages['date'], format='%Y%m%d')
    get_landingPages['sessions'] = pd.to_numeric(get_landingPages['sessions'])
    get_landingPages['view_id'] = pd.to_numeric(view_ids[0])
    
    try:
        bq.insert_data(get_landingPages, 'google_analytics.landing_pages')
        del get_landingPages
    except Exception as e:
        log.error(f"BQ error for {view_id} and days {start}")
        log.error(e)
    
    print(f"Loading data for {start} is done")
    # return get_landingPages


if __name__ == '__main__':
  main()

# exit()
# # SHEET_ID = ''

# # Full list of dimensions & metrics https://developers.google.com/analytics/devguides/reporting/core/dimsmets

# # DIMENSIONS = ['ga:source','ga:medium']
# # METRICS = ['ga:users','ga:sessions']


# # Events


# events = {
#     "DIMENSIONS": ['ga:date', 'ga:eventCategory', 'ga:eventAction', 'ga:eventLabel'],
#     "METRICS":['ga:totalEvents','ga:uniqueEvents','ga:eventValue','ga:avgEventValue','ga:sessionsWithEvent','ga:eventsPerSessionWithEvent', 'ga:users']
#     }

# lpages = {
#     "DIMENSIONS": ['ga:date','ga:landingPagePath', 'ga:channelGrouping', 'ga:pageTitle'],
#     "METRICS":['ga:sessions' ]
# }

# content_groups= {
#     "DIMENSIONS": ['ga:date','ga:contentGroup1'],
#     "METRICS":['ga:sessions' ]
# }

# pages = {
#     "DIMENSIONS": ['ga:date','ga:pagePath', 'ga:pageTitle'],
#     "METRICS":['ga:pageviews' ]
# }


# tables_list = [events]

# def main():
#     analytics = initialize_analyticsreporting()
#     response = get_report(analytics)
#     df = convert_to_dataframe(response)   #df = pandas dataframe
#     # export_to_sheets(df)                  #outputs to spreadsheet. comment to skip
#     print(df.head(50) ) 
#     # print(df.sort_values('ga:users', ascending = False) ) 
#     # print(df.describe(exclude=[object]))


# def initialize_analyticsreporting():
#     credentials = ServiceAccountCredentials.from_json_keyfile_name(KEY_FILE_LOCATION, SCOPES)

#   # Build the service object.
#     analytics = build('analyticsreporting', 'v4', credentials=credentials)

#     return analytics


# def get_report(analytics):
#     return analytics.reports().batchGet(
#         body={
#         'reportRequests': [
#         {
#             'viewId': VIEW_ID,
#             'dateRanges': [{'startDate': '7daysAgo', 'endDate': 'today'}],
#             'metrics': [{'expression':i} for i in pages['METRICS']],
#             'dimensions': [{'name':j} for j in pages['DIMENSIONS']]
#         }]
#         }
#     ).execute()
  
  
# def convert_to_dataframe(response):
    
#     for report in response.get('reports', []):
#         columnHeader = report.get('columnHeader', {})
#         dimensionHeaders = columnHeader.get('dimensions', [])
#         metricHeaders = [i.get('name',{}) for i in columnHeader.get('metricHeader', {}).get('metricHeaderEntries', [])]
#         finalRows = []

#         for row in report.get('data', {}).get('rows', []):
#             dimensions = row.get('dimensions', [])
#             metrics = row.get('metrics', [])[0].get('values', {})
#             rowObject = {}

#             for header, dimension in zip(dimensionHeaders, dimensions):
#                 rowObject[header] = dimension
            
#             for metricHeader, metric in zip(metricHeaders, metrics):
#                 rowObject[metricHeader] = metric

#             finalRows.append(rowObject)
            
#         dataFrameFormat = pd.DataFrame(finalRows)    
#     return dataFrameFormat      
  
# # def export_to_sheets(df):
# #     gc = pygsheets.authorize(service_file='client_secrets.json')
# #     sht = gc.open_by_key(SHEET_ID)
# #     wks = sht.worksheet_by_title('Sheet1')
# #     wks.set_dataframe(df,'A1')  

                           

# if __name__ == '__main__':
#   main()